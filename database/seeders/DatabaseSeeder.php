<?php

namespace Database\Seeders;

use App\Models\PortfolioItem;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create([
            'name' => 'admin',
            'email' => 'info@kasperversteeg.nl',
            'password' => Hash::make('flare99l')
        ]);

        PortfolioItem::factory()->create([
            'title' => 'NiksNuttigs.nl',
            'url' => 'https://www.niksnuttigs.nl',
            'img' => 'niksnuttigs.webp',
            'description' => 'De ReserV website is gemaakt als presentatie voor de applicatie zelf, het idee van de website is een soepele/simpele website die de nadruk legt op het bekijken van de demo van de app zelf. Het is een Single Page Application die volledig gemaakt is in Vue.js',
            'thumb' => 'nn-thumb.png'
        ]);
        PortfolioItem::factory()->create([
            'title' => 'ReserV Website',
            'url' => 'https://www.reserv-app.nl',
            'img' => 'reserv.webp',
            'description' => 'De ReserV website is gemaakt als presentatie voor de applicatie zelf, het idee van de website is een soepele/simpele website die de nadruk legt op het bekijken van de demo van de app zelf. Het is een Single Page Application die volledig gemaakt is in Vue.js',
            'thumb' => 'reserv-logo-full.png'
        ]);
        PortfolioItem::factory()->create([
            'title' => 'ReserV reserveringssoftware',
            'url' => 'https://demo.reserv-app.nl/',
            'img' => 'reserv-app.webp',
            'description' => 'De ReserV App is een applicatie die de reserveringsregistratie van het bedrijf makkelijker en sneller maakt. De App neemt zorgt ervoor dat iedereen makkelijk reserveringen toe kan voegen en denkt mee waar dat nodig is. Deze App is gemaakt in Vue.js in combinatie met Laravel ',
            'thumb' => 'reserv-logo.png'
        ]);
        PortfolioItem::factory()->create([
            'title' => 'Peluche',
            'url' => 'https://www.labradoodles.it',
            'img' => 'peluche.webp',
            'description' => 'Dit is een project in opdracht van de fokker van australian labradoodles. Het idee van de website is de gebruikers de sfeer van de fokker te geven, zodat het gelijk duidelijk is om wat voor soort fokker het gaat. Het is een wordpress website met aanpassingen.',
            'thumb' => 'peluche-thumb.png'
        ]);
        PortfolioItem::factory()->create([
            'title' => 'Pride and Joy',
            'url' => 'https://prideandjoyaustralianlabradoodles.nl/',
            'img' => 'pride.webp',
            'description' => 'Dit is een project in opdracht van de fokker van australian labradoodles. Het idee van de website is de gebruikers de sfeer van de fokker te geven, zodat het gelijk duidelijk is om wat voor soort fokker het gaat. Het is een wordpress website met aanpassingen.',
            'thumb' => 'pride-thumb.png'
        ]);
    }
}
