module.exports = {
    purge: [
        './resources/**/*.blade.php',
        './resources/**/*.js',
        './resources/**/*.vue',
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {
        container: {
            center: true,
            screens: {
                sm: "100%",
                md: "100%",
                lg: "1024px",
                xl: "1280px"
            }
        },
        fontFamily: {
            display: ['Raleway', 'sans-serif']
        },
        extend: {
            maxWidth: {
                '1/4': '25%',
                '1/2': '50%',
                '3/4': '75%'
            },
            colors: {
                primary: '#AF1B1B',
                secondary: '#575757',
                tertiary: '#606c76',
                text: '#343b42',
                red: {
                    'light': '#bb3333'
                },
                gray: {
                    'light': '#f5f5f5',
                    'darker': '#999'
                },
            }
        }
    },
    variants: {
        extend: {},
    },
    plugins: [],
}
