<?php

namespace App\Http\Controllers;

use App\Models\PortfolioItem;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function home()
    {
        $featured = PortfolioItem::orderBy('CREATED_AT', 'desc')->limit(5)->get();

        return view('pages.home', ['featured' => $featured]);
    }

    public function portfolio()
    {
        return view('pages.portfolio');
    }

    public function overmij()
    {
        return view('pages.overmij');
    }
}
