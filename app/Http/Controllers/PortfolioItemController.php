<?php

namespace App\Http\Controllers;

use App\Http\Requests\PortfolioStoreRequest;
use App\Models\File;
use App\Models\PortfolioItem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class PortfolioItemController extends Controller
{
    const THUMBNAIL_PATH = 'public/img/thumbnails/';
    const IMAGE_PATH = 'public/img/thumbnails/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $items = PortfolioItem::all();
        return view('pages.portfolio', compact('items'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('pages.portfolio-item.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return View
     */
    public function store(PortfolioStoreRequest $request)
    {
        $item = PortfolioItem::create($request->all());
        // Create thumbnail file.
        $uploadedThumbnail = $request->file('thumbnail');
        if ($uploadedThumbnail) {

            $filename = 'thumb-' . $item->getName() . '.' . $uploadedThumbnail->getClientOriginalExtension();
            $uploadedThumbnail->storeAs('img/thumbnails', $filename, ['disk' => 'public_folder']);

            $this->storeFile($filename);
            $item->setThumbnail($filename);
        }

        // Create image file.
        $uploadedImage = $request->file('image');
        if ($uploadedImage) {

            $filename = 'img-' . $item->getName() . '.' . $uploadedImage->getClientOriginalExtension();
            $uploadedImage->storeAs('img', $filename, ['disk' => 'public_folder']);

            $this->storeFile($filename);
            $item->setImage($filename);
        }

        $item->save();

        return redirect('admin')->with('succes', 'succes');
    }

    public function storeFile($name)
    {
        $file = new File();
        $file->filename = $name;
        $file->user()->associate(auth()->user());
        return $file->save();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\PortfolioItem $portfolioItem
     * @return View
     */
    public function show(PortfolioItem $portfolioItem)
    {
        return view('pages.portfolio-single', compact('portfolioItem'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\PortfolioItem $portfolioItem
     * @return Response
     */
    public function edit(PortfolioItem $portfolioItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param \App\PortfolioItem $portfolioItem
     * @return Response
     */
    public function update(Request $request, PortfolioItem $portfolioItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\PortfolioItem $portfolioItem
     * @return Response
     */
    public function destroy(PortfolioItem $portfolioItem)
    {
        //
    }
}
