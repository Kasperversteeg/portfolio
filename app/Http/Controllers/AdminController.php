<?php

namespace App\Http\Controllers;

use App\Models\PortfolioItem;
use Illuminate\Contracts\Support\Renderable;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $items = PortfolioItem::all()->sortDesc();
        return view('pages.admin', compact('items'));
    }
}
