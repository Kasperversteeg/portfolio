<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PortfolioItem extends Model
{
    use HasFactory;

    protected $guarded = [
        'thumb',
        'img'
    ];

    function getName()
    {
        $name = $this->getAttribute('title');

        return str_replace(' ', '_', strtolower($name));
    }

    function setThumbnail($path)
    {
        $this->setAttribute('thumb', $path);
        return $this;
    }

    function setImage($path)
    {
        $this->setAttribute('img', $path);
        return $this;
    }

}
