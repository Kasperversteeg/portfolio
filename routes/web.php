<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\PortfolioItemController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Controller::class, 'home']);
Route::get('/portfolio', [PortfolioItemController::class, 'index']);
Route::get('/overmij', [Controller::class, 'overmij']);

Route::get('portfolio/{portfolioItem}/show', [PortfolioItemController::class, 'show']);
Route::group(['middleware' => ['auth']], function () {
    Route::get('portfolio/create', [PortfolioItemController::class, 'create'])->name('portfolio_item.create');
    Route::post('portfolio/store', [PortfolioItemController::class, 'store'])->name('portfolio_item.store');
});

Auth::routes(['register' => false]);

Route::get('/admin', [App\Http\Controllers\AdminController::class, 'index'])->name('admin');
