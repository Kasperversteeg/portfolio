import Vue from "vue";
import store from './store';
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import {mapState} from 'vuex';

window.Vue = require('vue');


Vue.component('Navigation', require('./components/Navigation.vue').default);
Vue.component('Portfolio', require('./components/Portfolio.vue').default);
Vue.component('Modal', require('./components/Modal.vue').default);
Vue.component('Icon', require('./components/Icon.vue').default);

const app = new Vue({
    el: '#app',
    store,
    computed: {
        ...mapState({
            isModalShowing: state => state.modal.modalShowing
        }),
    }
});
