<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- fonts --}}
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap"
          rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css?family=Source+Code+Pro:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    {{-- custom files --}}
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon-16x16.png')}}"/>
    <link rel="stylesheet" href="{{ asset('css/app.css')}}"/>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js')}}" defer></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-70JS4FNPHQ"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'G-70JS4FNPHQ');
    </script>
    <title>Kasper Versteeg - @yield('title')</title>
</head>

<body>
<div id="app" class="font-display min-h-screen h-full flex flex-col text-lg text-text">
    {{-- navigation --}}
    <nav class="border-b">
        <div class="container flex flex-row justify-between">
            <div class="w-3/4 md:w-1/2 box-border">
                <a href="/">
                    <img class="py-4 lg:w-3/4 xl:w-1/2 " src="{{ asset('img/kv-logo-full.svg')}}" alt="">
                </a>
            </div>
            <div class="min-h-full w-1/4 md:w-1/2 px-2">
                <navigation/>
            </div>
        </div>
    </nav>

    {{-- main container --}}
    <div>
        {{-- insert page content --}}
        @yield('content')
    </div>

    {{-- footert --}}
    <footer class="mt-auto">
        <div class="relative bg-red-light">
            <div class="container py-12 text-center">
                <h2 class="text-4xl text-bold text-white pb-4">Nieuwe website nodig?</h2>
                <a class="btn btn-white" href="mailto:info@kasperversteeg.nl">Neem contact op</a>
            </div>
        </div>
        <div class="w-full px-4 bg-primary text-white py-12">
            <div class="container grid grid-cols-1 md:grid-cols-3 text-center md:text-left">
                <div class="text-sm hidden md:block">
                    <a href="/"><img class="max-w-1/2" src="{{ asset('img/kv-logo-full-white.svg') }}" alt="Logo"></a>
                    <p class="mt-4">Kasper Versteeg</p>
                    <p>Webdeveloper</p>
                </div>
                <div class="mx-auto mb-2">
                    <h5 class="text-xl">Contact:</h5>
                    <ul class="flex flex-col items-center md:items-start">
                        <li class="hover:underline py-3">
                            <a class="flex flex-row items-center" href="tel:+31611119944">
                                <img class="w-8"
                                     src="/img/pictograms/pict-tel.svg"
                                     alt="">
                                <p class="inline-block w-90 px-2">+31 6 11 11 99 44</p></a>
                        </li>
                        <li class="hover:underline  py-3">
                            <a class="flex flex-row items-center" href="mailto:info@kasperversteeg.nl"> <img
                                    class="w-8" src="/img/pictograms/pict-mail.svg" alt="">
                                <p class="w-90 px-2">info@kasperversteeg.nl</p></a>
                        </li>
                    </ul>
                </div>
                <div class="mx-auto">
                    <h5 class="text-xl md:text-base">Socials:</h5>
                    <ul class="flex flex-row">
                        <li class="w-16 p-2">
                            <a href="https://www.facebook.com/kasper.versteeg.1">
                                <img src="/img/pictograms/pict-fb.svg"
                                     alt="Facebook"></a>
                        </li>
                        <li class="w-16 p-2">
                            <a href="https://www.linkedin.com/in/kasper-versteeg">
                                <img src="/img/pictograms/pict-li.svg"
                                     alt="LinkedIn"></a>
                        </li>
                        <li class="w-16 p-2">
                            <a href="https://github.com/Kasperversteeg">
                                <img class=hover:fill-primary
                                     src="/img/pictograms/pict-gh.svg"
                                     alt="Github"></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="w-full flex pt-4">
                <p class="mx-auto text-xs">copyright © KasperVersteeg - powered bij <a href="https://laravel.com/">laravel</a>
                </p>
            </div>
        </div>
    </footer>
    <modal v-show="isModalShowing"/>

</div>
</body>
<script>
    const imgOffset = document.querySelector('#kv-hero-v').getBoundingClientRect().bottom
    const wrapperOffset = document.querySelector('#kv-hero-wrapper').getBoundingClientRect().bottom
    const startPoint = wrapperOffset - imgOffset

    window.addEventListener('scroll', function () {
        let offset = (window.pageYOffset / 2) + startPoint;
        const img = document.querySelector('#kv-hero-v')
        img.style.bottom = offset + 'px';
    });
</script>
</html>
