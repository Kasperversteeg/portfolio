@extends('layout.app')

@section('content')
    <div class="flex items-center justify-center min-h-screen bg-gray-100">
        <div class="px-8 py-6 mt-4 text-left bg-white shadow-lg w-1/4">
            <h3 class="text-2xl font-bold text-center">Login </h3>

            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="mt-4">
                    <div>
                        <label class="block" for="email">{{ __('E-Mail Address') }}</label>
                        <input name="email" type="text" placeholder="Email" value="{{ old('email') }}"
                               class="w-full px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>
                    <div class="mt-4">
                        <label class="block">{{ __('Password') }}</label>
                        <input name="password" type="password" placeholder="Password"
                               required autocomplete="current-password"
                               class="w-full px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600 @error('password') border-red @enderror">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="flex items-baseline justify-between">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember"
                                   id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                        <button class="px-6 py-2 mt-4 text-white bg-blue-600 rounded-lg hover:bg-blue-900 ">Login
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
