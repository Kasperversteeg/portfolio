{{-- get master layout --}}
@extends('layout.master')

{{-- insert title --}}
@section('title', 'Over mij')

{{-- main content --}}
@section('content')
    <div class="pt-16 pb-4 container">
        <div class="flex flex-col md:flex-row">
            <div class="flex-shrink md:w-1/2 md:py-4 md:px-24 flex items-center">
                <img class="max-w-full  rounded-2xl" src="img/om-kasper.webp">
            </div>
            <div class="flex-none md:w-1/2 flex-1 py-12 md:p-12">
                <h1>Over mij</h1>
                <h2 class="pb-3 text-primary">Ik ben Kasper Versteeg een fullstack webdeveloper.</h2>
                <p class="leading-relaxed text-lg mb-4">
                    Nadat ik 15 jaar als Kok aan het werk ben geweest in de horeca, ben ik omgeschoold tot multimedia
                    expert. Ik ben op het moment werkzaam als junior webdeveloper bij <a
                        class="text-primary font-medium hover:underline" href="https://www.develto.nl"
                        target="_blank">Develto</a>.
                </p>
                <p>Ik heb de opleiding Communication & Multimedia Design afgerond op de Noordelijke hogeschool in
                    Leeuwarden. Hier heb ik mij gespecialiseerd in het passend maken van de website/software bij de
                    vraag van de opdrachtgever en de eindgebruiker. Het doel van het project en de
                    gebruiksvriendelijkheid staan bij mij voorop bij het ontwikkelen van nieuwe projecten. Wilt u mij
                    inhuren voor een nieuw project?
                </p>
                <a class="my-4 btn btn-primary w-full text-center md:w-auto md:text-left"
                   href="mailto:info@kasperversteeg.nl"> Neem
                    contact op</a>
            </div>
        </div>
    </div>

    <div class="divider"></div>

    <div class="flex container md:flex-row flex-col">
        <div class="flex-1 flex md:justify-center">
            <div>
                <h2>Contact:</h2>
                <ul>
                    <li class="p-2 hover:underline">
                        <a href="tel:+31611119944">
                            <icon id="tel" name="+31 6 11 11 99 44"></icon>
                        </a>
                    </li>
                    <li class="p-2 hover:underline">
                        <a href="mailto:info@kasperversteeg.nl">
                            <icon id="mail" name="info@kasperversteeg.nl"></icon>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="flex-1 flex md:justify-center">
            <div>
                <h2>Socials:</h2>
                <ul>
                    <li class="p-2 hover:underline"><a href="https://www.facebook.com/kasper.versteeg.1">
                            <icon id="facebook" name="Facebook"></icon>
                        </a></li>
                    <li class="p-2 hover:underline"><a href="https://www.linkedin.com/in/kasper-versteeg">
                            <icon id="linkedin" name="LinkedIn"></icon>
                        </a></li>
                    <li class="p-2 hover:underline"><a href="https://github.com/Kasperversteeg">
                            <icon id="github" name="Github"></icon>
                        </a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="divider"></div>

    <div class="pt-4 pb-12 container">
        <h2>Skillset</h2>
        <p>Buiten mijn Communicatie en Usability vaardigheden beheers ik de volgende talen/frameworks.</p>
        <ul class="grid grid-cols-2 md:grid-cols-10 gap-4 py-8">
            <li class="kv-skill-item">
                <img src="/img/pictograms/php-1.svg" alt="">
            </li>
            <li class="kv-skill-item">
                <img src="/img/pictograms/laravel-2.svg" alt="">
            </li>
            <li class="kv-skill-item">
                <img src="/img/pictograms/javascript-1.svg" alt="">
            </li>
            <li class="kv-skill-item">
                <img src="/img/pictograms/vue-9.svg" alt="">
            </li>
            <li class="kv-skill-item">
                <img src="/img/pictograms/tailwindcss.svg" alt="">
            </li>
            <li class="kv-skill-item">
                <img src="/img/pictograms/drupal-2.svg" alt="">
            </li>
            <li class="kv-skill-item">
                <img src="/img/pictograms/docker-1.svg" alt="">
            </li>
            <li class="kv-skill-item">
                <img src="/img/pictograms/git.svg" alt="">
            </li>
            <li class="kv-skill-item">
                <img src="/img/pictograms/sass-1.svg" alt="">
            </li>
        </ul>
    </div>




@endsection
