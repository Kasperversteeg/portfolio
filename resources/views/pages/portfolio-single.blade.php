{{-- get master layout --}}
@extends('layout.master')

{{-- insert title --}}
@section('title', 'Item')

{{-- main content --}}
@section('content')

    <div class="container py-12">
        <div class="pb-6">
            <h1>{{ $portfolioItem->title }}</h1>
            <ul class="text-tertiary">
                <li class="breadcrumb"><a href="/portfolio">Portfolio</a></li>
                <li class="breadcrumb">/</li>
                <li class="breadcrumb font-semibold">{{ $portfolioItem->title }}</li>
            </ul>
        </div>
        <div class="flex md:flex-row flex-col py-4">
            <div class="flex-1 px-4">
                <div class="img-wrapper overflow-hidden">
                    <a target="_blank" href="{{ $portfolioItem->url }}">
                        <img class="scale overflow-hidden" src="{{ '/img/' . $portfolioItem->img }}"
                             alt="{{  $portfolioItem->title }}"></a>
                </div>
            </div>
            <div class="flex-1 p-6 border">
                <h2>Categorie: Website</h2>
                <p class="py-6">{{ $portfolioItem->description }}</p>
                <a class="btn btn-primary mt-4 md:mt-0 text-center" target="_blank" href="{{ $portfolioItem->url }}">Bekijk
                    live</a>
            </div>
        </div>

    </div>


@endsection
