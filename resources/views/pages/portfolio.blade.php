{{-- get master layout --}}
@extends('layout.master')

{{-- insert title --}}
@section('title', 'Portfolio')

{{-- main content --}}
@section('content')
    <div id="kv-portfolio" class="py-12 container">
        <h1>Projecten</h1>
        <p>Een overzicht van gemaakte projecten</p>
        <div class="">
            <portfolio :items="{{ json_encode($items) }}"></portfolio>
        </div>
    </div>
@endsection
