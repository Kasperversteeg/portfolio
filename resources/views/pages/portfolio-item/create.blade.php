@extends('layout.app')

@section('content')
    <div class="flex items-center justify-center min-h-screen bg-gray-100">
        <div class="px-8 py-6 mt-4 text-left bg-white shadow-lg w-1/2">
            <h3 class="text-2xl font-bold text-center">Portfolio item maken</h3>

            <form method="POST" action="{{ route('portfolio_item.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="mt-4">
                    <div>
                        <label class="block" for="title">{{ __('Title') }}</label>
                        <input name="title" type="text" placeholder="titel" value="{{ old('title') }}"
                               class="w-full px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600 @error('title') border-red-600 @enderror">
                        @error('title')
                        <span class="text-red-600 text-sm" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="mt-4">
                    <label class="block">{{ __('Link') }}</label>
                    <input name="url" type="text" placeholder="http://domein.nl" value="{{ old('url') }}"
                           class=" w-full px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1
                           focus:ring-blue-600 @error('url') border-red-600 @enderror">
                    @error('url')
                    <span class="text-red-600 text-sm" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="mt-4">
                    <label class="block">{{ __('Thumbnail') }}</label>
                    <input name="thumbnail" type="file"
                           class="w-full px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600 @error('thumbnail') border-red-600 @enderror">
                    @error('thumbnail')
                    <span class="text-red-600 text-sm" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="mt-4">
                    <label class="block">{{ __('Image') }}</label>

                    <input name="image" type="file"
                           class="w-full px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600 @error('image') border-red-600 @enderror">
                    @error('image')
                    <span class="text-red-600 text-sm" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="mt-4">
                    <label class="block">{{ __('Description') }}</label>
                    <textarea name="description" id="" cols="30" rows="4"
                              value="{{ old('description') }}"
                              class="w-full px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600 @error('description') border-red-600 @enderror">
                    </textarea>
                    @error('description')
                    <span class="text-red-600 text-sm" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="flex items-baseline justify-between">
                    <button class="px-6 py-2 mt-4 text-white bg-blue-600 rounded-lg hover:bg-blue-900">Maak
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
