{{-- get master layout --}}
@extends('layout.master')

{{-- insert title --}}
@section('title', 'Home')

{{-- main content --}}
@section('content')
    <div class="kv-hero-section">
        <div class="px-6 md:px-none container flex items-center h-full">
            <div id="kv-hero-wrapper" class="hidden md:block w-1/2 relative h-full overflow-hidden">
                <img id="kv-hero-k" src="/img/kv-k.svg" alt="kasperversteeg Hero K">
                <img id="kv-hero-v" src="/img/kv-v.svg" alt="kasperversteeg Hero K">
            </div>
            <div class="w-screen md:w-1/2">
                <p class="text-gray-darker text-2xl">Ik ben</p>
                <h1 class="kv-title"><span class="text-primary">Kasper</span> <br/> Versteeg</h1>
                <p class="text-4xl text-medium text-gray-darker leading-normal">Fullstack webdeveloper en <br/>
                    usability expert.</p>
            </div>
        </div>
    </div>

    {{-- ReserV section  --}}
    <div class="py-16 bg-gray-light">
        <div class="container">
            <div class="flex flex-col-reverse md:flex-row items-center">
                <div class="md:w-1/2 leading-relaxed text-lg md:pr-8">
                    <h2>ReserV</h2>
                    <p class="mb-4">Ik ben sinds 2019 bezig met het ontwikkelen van mijn ReserV
                        reserverings software. Dit project bestaat uit een <a class="text-primary"
                                                                              href="https://www.laravel.com">Laravel</a>
                        API en een
                        <a href="https://vuejs.org" class="text-primary">Vue</a> single page
                        applicaion. Het systeem blinkt uit in gebruiksgemak doordat het voor en door horeca personeel
                        gemaakt is. Door regelmatig onderzoek te doen bij de doelgroep sluit de software precies aan bij
                        de wensen en eisen van de gebruikers. </p>
                    <p class="mb-4">Benieuwd geworden naar het project?</p>
                    <a class="btn btn-primary" href="https://demo.reserv-app.nl" target="_blank">Bekijk het project</a>
                </div>
                <div class="md:w-1/2">
                    <div class="p-6">
                        <img src="/img/reserv-app.webp" alt="ReserV reserveringssysteem">
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- featured portfolio items --}}
    <div class="py-16">
        <div class="container">
            <h2>Projecten</h2>
            <p class="leading-relaxed text-xl">Hieronder ziet u een overzicht van mijn meest recente projecten, mocht u
                meer willen zien dan kunt u
                verder
                kijken op de <a class="text-primary font-medium hover:underline" href="portfolio">portfolio</a> pagina
            </p>
            <portfolio class="mt-4" :items="{{ json_encode($featured) }}"></portfolio>
        </div>
    </div>

@endsection
